package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/ErrorHandler")
public class ErrorHandler extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
       try{
        processError(request, response);}
        catch (ServletException e)
        {
            log(e.getMessage());
        }
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        try{
            processError(request, response);}
        catch (ServletException e)
        {
            log(e.getMessage());
        }
    }

    private void processError(HttpServletRequest request,
                              HttpServletResponse response) throws IOException, ServletException {
        // Analyze the servlet exception
        Throwable throwable = (Throwable) request
                .getAttribute("javax.servlet.error.exception");
        Integer statusCode = (Integer) request
                .getAttribute("javax.servlet.error.status_code");
        String servletName = (String) request
                .getAttribute("javax.servlet.error.servlet_name");
        if (servletName == null) {
            servletName = "Unknown";
        }
        String requestUri = (String) request
                .getAttribute("javax.servlet.error.request_uri");
        if (requestUri == null) {
            requestUri = "Unknown";
        }

        StringBuilder error= new StringBuilder();

        if(statusCode != 500){
            error.append("Error Details: ");
            error.append("Status Code: ").append(statusCode);
          error.append("Requested URI: ").append(requestUri);

        }else{
            error.append("Exception Details: ");
            error.append("Servlet Name: ").append(servletName);
            error.append("Exception Name: ").append(throwable.getClass().getName());
            error.append("Requested URI: ").append(requestUri);
            error.append("Exception Message: ").append(throwable.getMessage());

        }


        request.setAttribute("error", error);
        RequestDispatcher rd = request.getRequestDispatcher("somethingWentWrong.jsp");
        rd.forward(request, response);
    }
}