package controller.user;

import model.Keyword;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.KeywordService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/admin-page")
public class AdminServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(AdminServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

       //check if user is logged in and is admin

        HttpSession session = req.getSession(false);

        if(session!=null&&session.getAttribute("email") != null){
            if(Integer.parseInt(String.valueOf(session.getAttribute("role")))!=1){
                resp.sendRedirect("notAllowed.html");
            }

            else{

            try {
                String displayedName = new UserService().getUserLogin((String) session.getAttribute("email"));
                if (displayedName.equals(""))
                    req.setAttribute("displayedName", session.getAttribute("email"));
                else
                    req.setAttribute("displayedName", displayedName);

                    ArrayList<User> users;
                    users = (ArrayList<User>) new UserService().getAllUsers();
                    req.setAttribute("users", users);
                    ArrayList<Keyword> keywords;
                    keywords= new KeywordService().getAllKeywords();
                    req.setAttribute("keywords", keywords);
                    RequestDispatcher rd = req.getRequestDispatcher("adminPage.jsp");
                    rd.forward(req, resp);


            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");

            }

            }



        } else

            { resp.sendRedirect("notSignedIn.html");  }

    }
}



