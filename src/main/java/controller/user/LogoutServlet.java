package controller.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(LogoutServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
try{
        if(req.getSession(false)!=null){
        req.getSession(false).invalidate();
        req.getRequestDispatcher("home").forward(req, resp);
        log.info("logout");}
    }
    catch (ServletException e){
    log.error(e.getMessage());
    resp.sendRedirect("ErrorHandler");
    }
    }
}
