package controller.user;

import com.sun.net.httpserver.HttpsServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/change-role")
public class ChangeRoleServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(ChangeRoleServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            int userId = Integer.parseInt(req.getParameter("user"));
            int userRole = Integer.parseInt(req.getParameter("userRole"));
            new UserService().changeRole(userId, userRole);
            resp.sendRedirect("admin-page");
        } catch (SQLException | IOException e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }
    }
}
