package controller.user;

import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    private static final Logger log = (Logger) LogManager.getLogger(RegisterServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String userLogin = req.getParameter("userLogin");
        String userPassword = req.getParameter("password");
        String usesEmail  = req.getParameter("email");

        //roles 1-admin, 2-moderator, 3-simple user
        boolean makeAdmin = Boolean.parseBoolean(req.getParameter("makeAdmin"));
        User user;

        try {
            if (makeAdmin){
                user= User.newBuilder().build(userLogin, userPassword, 1,usesEmail, true);
            } else{
                user = User.newBuilder().build(userLogin, userPassword, 2,usesEmail, true);
            }

            boolean success= new UserService().registerUser(user);
            if (success)
            req.setAttribute("success", "1");
            else

                req.setAttribute("success", "0");
            RequestDispatcher rd=req.getRequestDispatcher("register.jsp");
            rd.forward(req,resp);
        } catch (SQLException | IOException e) {

            log.error(e.getMessage());

        }


    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher view = req.getRequestDispatcher("register.jsp");

        try{
        view.forward(req, resp);}
        catch (ServletException e){
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }
    }
}
