package controller.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/ban-user")
public class BanUserServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(BanUserServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);

        if (session != null && session.getAttribute("email") != null) {
            if (Integer.parseInt(String.valueOf(session.getAttribute("role"))) != 1) {
                resp.sendRedirect("notAllowed.html");

            }


            else {


                try {
                    String displayedName = new UserService().getUserLogin((String) session.getAttribute("email"));
                    if (displayedName.equals(""))
                        req.setAttribute("displayedName", session.getAttribute("email"));
                    else
                        req.setAttribute("displayedName", displayedName);

                    int userId = Integer.parseInt(req.getParameter("user"));
                    new UserService().banUser(false, userId);
                    resp.sendRedirect("admin-page");
                } catch (SQLException | IOException e) {
                    log.error(e.getMessage());
                    resp.sendRedirect("ErrorHandler");
                }
            }
        } else{       resp.sendRedirect("notAllowed.html");}
    }
}