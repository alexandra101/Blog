package controller.user;

import dao.UserDAO;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(LoginServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        //get credentials
        String userEmail = req.getParameter("userEmail");
        String userPassword = req.getParameter("password");

        boolean isAuthenticated = false;
        //roles 1-admin, 2-moderator, 3-simple user
        RequestDispatcher rd;
        User user = User.newBuilder().build(null, userPassword, 3,userEmail, true);
        try {

        if (user==null){
            req.setAttribute("failedAuth", 1);
            rd = req.getRequestDispatcher("login.jsp");
            rd.forward(req, resp);
        }

        else {
            isAuthenticated = new UserService().authenticateUser(user);
            if (isAuthenticated) {
                User authUser = new UserService().getUserFromDB(userEmail);
                req.getSession(true);
                req.getSession().setAttribute("email", userEmail);
                req.getSession().setAttribute("role", authUser.getUserRole());
                req.getSession().setMaxInactiveInterval(120);
                rd = req.getRequestDispatcher("profile");

            } else {
                req.setAttribute("failedAuth", 1);
                rd = req.getRequestDispatcher("login.jsp");
            }

            rd.forward(req, resp);

        }

        }catch (SQLException | IOException e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }

    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher view = req.getRequestDispatcher("login.jsp");
        try {
            view.forward(req, resp);
        }
        catch (ServletException e){
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }


    }
}

