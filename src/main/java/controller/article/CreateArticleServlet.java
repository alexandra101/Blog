package controller.article;

import model.Article;
import model.Keyword;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.KeywordService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;


@WebServlet(urlPatterns = {"/create-article"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB


public class CreateArticleServlet extends HttpServlet {

    private static final Logger log = (Logger) LogManager.getLogger(CreateArticleServlet.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


      //get article from form fields
      //create it in db and redirect to profile
        try {
            new ArticleService().createOrUpdateArticle(createArticleFromFields(req), true, false);
            resp.sendRedirect("profile");
        }
                catch (SQLException| IOException | NullPointerException e ) {

            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }




    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //check if user is signed in
        HttpSession session = req.getSession(false);
        if(session==null||session.getAttribute("email") == null){

            try {
              resp.sendRedirect("notSignedIn.html");
            } catch (IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");

            }
        } else

        if(session.getAttribute("email") != null){

            try {

                req.setAttribute("displayedName", new UserService().getUserLogin((String) session.getAttribute("email")));
                //get all keywords from db to display it on form
                ArrayList<Keyword> keywords;
                keywords=new KeywordService().getAllKeywords();
                req.setAttribute("keywords", keywords);
                RequestDispatcher requestDispatcher = req.getRequestDispatcher("createArticle.jsp");
                requestDispatcher.forward(req, resp);

            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }
        }

          }

    private Article createArticleFromFields(HttpServletRequest req) throws IOException, ServletException, SQLException {

        String title = req.getParameter("title");
        String text = req.getParameter("text");

        Timestamp dateCreated;
        Timestamp datePublished;
        Timestamp dateUpdated;

        User author;

        boolean isPosted = Boolean.parseBoolean(req.getParameter("isPosted"));

        String   imagePath = null;

        String[]keywords = req.getParameterValues("keywords");
        ArrayList<Keyword> articleKeywords1=new ArrayList<>();

        author = new UserService().getUserFromDB(String.valueOf(req.getSession(false).getAttribute("email")));

        for (String keyword : keywords) {
                articleKeywords1.add(new KeywordService().getKeywordsById(Integer.parseInt(keyword)));
            }

        //get image
        Part part = req.getPart("image");
        if (!part.getSubmittedFileName().equals("")) {
                String appPath = req.getServletContext().getRealPath("/");
                imagePath =  new ArticleService().saveImage(part, appPath);
            }

       dateCreated = new Timestamp(new Date().getTime());
       dateUpdated = new Timestamp(new Date().getTime());
       datePublished = new Timestamp(new Date().getTime());

       return  Article.newBuilder().build(title, text, dateCreated, datePublished, dateUpdated, author, imagePath, isPosted, articleKeywords1);
    }



}
