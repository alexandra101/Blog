package controller.article;

import dao.UserDAO;
import model.Article;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {"/profile"})
public class GetAllUserArticlesServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(GetAllUserArticlesServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        HttpSession session = req.getSession(false);
        String userEmail=null;
        int page=1;
        int recordsPerPage = 3 ;

        //check if user is signed in
        if(session!=null&&session.getAttribute("email") != null){
            try {
                userEmail = String.valueOf(session.getAttribute("email"));
                req.setAttribute("displayedName", new UserService().getUserLogin(userEmail));
            } catch (SQLException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }


        ArrayList<Article>  articles = null;
        int userId;
        //get user id by saved in session email
        //get user articles by userId(which is foreign key in db)
        try {
            if(req.getParameter("page") != null)
                page = Integer.parseInt(req.getParameter("page"));

            userId=new UserDAO().getUserFromDB(userEmail).getUserId();

            articles= (ArrayList<Article>) new ArticleService().getAllUserArticlesInRange(userId,3,(page-1)*recordsPerPage);

            int noOfRecords =new ArticleService().getUserArticlesNumber(userId);
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

            req.setAttribute("noOfPages", noOfPages);
            req.setAttribute("currentPage", page);

            req.setAttribute("articles", articles);
            RequestDispatcher rd=req.getRequestDispatcher("userProfile.jsp");
            rd.forward(req,resp);
        } catch (SQLException | IOException e) {
            log.error(e.getMessage());
        }
    }

    else {


            try {
                resp.sendRedirect("notSignedIn.html");
            } catch (Exception e){
                log.error(e.getMessage());
            }
        }

    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     try{
      doGet(req, resp);
     }
     catch (IOException | ServletException e){
         log.error(e.getMessage());
         resp.sendRedirect("ErrorHandler");
     }
    }
}
