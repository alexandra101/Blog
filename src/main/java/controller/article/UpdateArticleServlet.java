package controller.article;
import model.Article;
import model.Keyword;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.KeywordService;
import service.UserService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

@WebServlet(urlPatterns = { "/update-article"})
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 10,      // 10MB
        maxRequestSize = 1024 * 1024 * 50)   // 50MB

public class UpdateArticleServlet extends HttpServlet {

    private static final Logger log = (Logger) LogManager.getLogger(UpdateArticleServlet.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //get article updated fields, check if image is deleted
        //update article in db
        try {
            boolean deleteImage = Boolean.parseBoolean(req.getParameter("deleteImage"));
            Article article = createArticleFromFields(req);
            new ArticleService().createOrUpdateArticle(article, false, deleteImage);
            resp.sendRedirect("profile");
        } catch (SQLException |IOException e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //check if user is logged in

        HttpSession session = req.getSession(false);
        if(session==null||session.getAttribute("email") == null){
            try {
                resp.sendRedirect("notSignedIn.html");
            } catch (Exception e) {
                log.error(e.getMessage());


            }
        }

        else{

        if(session.getAttribute("email") != null){
            try {

                int articleId = Integer.parseInt(req.getParameter("article"));
                req.setAttribute("displayedName", new UserService().getUserLogin((String) session.getAttribute("email")));
                Article article;
                ArrayList<Keyword> keywords;
                    keywords=new KeywordService().getAllKeywords();
                    req.setAttribute("keywords", keywords);
                    article = new ArticleService().getArticleById(articleId);
                    req.setAttribute("article", article);
                    RequestDispatcher rd=req.getRequestDispatcher("updateArticle.jsp");
                    rd.forward(req, resp);    }

                    catch (SQLException | IOException e) {
                    log.error(e.getMessage());
                    resp.sendRedirect("ErrorHandler");

            }
        }
        }


        }




    private Article createArticleFromFields(HttpServletRequest req) throws IOException, ServletException, SQLException {

        int articleId = Integer.parseInt(req.getParameter("article"));

        String title = req.getParameter("title");
        String text = req.getParameter("text");
        Timestamp dateCreated;
        Timestamp datePublished;
        Timestamp dateUpdated;
        User author = null;
        boolean isPosted = Boolean.parseBoolean(req.getParameter("isPosted"));
        String   imagePath = null;

        String[]keywords = req.getParameterValues("keywords");
        ArrayList<Keyword> articleKeywords1=new ArrayList<>();


        for (String keyword : keywords) {
            articleKeywords1.add(new KeywordService().getKeywordsById(Integer.parseInt(keyword)));
        }


        Part part = req.getPart("image");


        if (!part.getSubmittedFileName().equals("")) {
            //get image
            String appPath = req.getServletContext().getRealPath("");
           imagePath =  new ArticleService().saveImage(part, appPath);
        }


        try {
            author = new UserService().getUserFromDB(String.valueOf(req.getSession(false).getAttribute("email")));
        } catch (SQLException e) {
            log.error(e.getMessage());
        }

        dateCreated = new Timestamp(new Date().getTime());
        dateUpdated = new Timestamp(new Date().getTime());
        datePublished = new Timestamp(new Date().getTime());
        return   Article.newBuilder().build(articleId, title, text, dateCreated, datePublished, dateUpdated, author, imagePath, isPosted, articleKeywords1);
    }

}
