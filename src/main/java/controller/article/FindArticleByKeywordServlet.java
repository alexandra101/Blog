package controller.article;

import model.Article;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.ErrorHandler;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.invoke.WrongMethodTypeException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/search-keyword")
public class FindArticleByKeywordServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(FindArticleByKeywordServlet.class);

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if(session!=null&&session.getAttribute("email") != null){
            try {
                //set user displayed name
                String displayedName = new UserService().getUserLogin((String) session.getAttribute("email"));
                if (displayedName.equals(""))
                    req.setAttribute("displayedName", session.getAttribute("email"));
                else
                    req.setAttribute("displayedName", displayedName);
            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }
        }
        //get keyword
        //find articles that have this keyword and show them
        ArrayList<Article> articles;
        try {
        int searchKeyword = Integer.parseInt(req.getParameter("keyword"));
            articles= (ArrayList<Article>) new ArticleService().findArticleByKeyword(searchKeyword);
            req.setAttribute("articles", articles);
            RequestDispatcher rd=req.getRequestDispatcher("searchResults.jsp");
            rd.forward(req,resp);

        } catch (SQLException| IOException | WrongMethodTypeException  | NullPointerException e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }
    }
}
