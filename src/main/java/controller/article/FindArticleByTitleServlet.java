package controller.article;

import model.Article;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/search")
public class FindArticleByTitleServlet extends HttpServlet {

    private static final Logger log = (Logger) LogManager.getLogger(FindArticleByTitleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        if(session!=null&&session.getAttribute("email") != null){
            try {
                //set user displayed name
                String displayedName = new UserService().getUserLogin((String) session.getAttribute("email"));
                if (displayedName.equals(""))
                    req.setAttribute("displayedName", session.getAttribute("email"));
                else
                    req.setAttribute("displayedName", displayedName);
            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }
        }

        //find article that has wanted words in title
        String searchTitle = req.getParameter("search");
        ArrayList<Article> articles;
        try {
            articles= (ArrayList<Article>) new ArticleService().findArticleByTitle(searchTitle);
            if (!articles.isEmpty()) {
                req.setAttribute("articles", articles);
            }


            else
            {
                req.setAttribute("nothingIsFound", "1");

        }
                RequestDispatcher rd=req.getRequestDispatcher("searchResults.jsp");
                 rd.forward(req,resp);

        } catch (SQLException |  IOException | NullPointerException  e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");

        }

    }
}
