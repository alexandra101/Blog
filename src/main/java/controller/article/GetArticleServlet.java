package controller.article;

import model.Article;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/article")
public class GetArticleServlet extends HttpServlet {

    private static final Logger log = (Logger) LogManager.getLogger(GetArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //check if user is logged in

        HttpSession session = req.getSession(false);
        if(session!=null&&session.getAttribute("email") != null){
            try {
                //set user displayed name
                String displayedName = new UserService().getUserLogin((String) session.getAttribute("email"));
                if (displayedName.equals(""))
                    req.setAttribute("displayedName", session.getAttribute("email"));
                else
                    req.setAttribute("displayedName", displayedName);
            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }
        }
        Article article;
        try {
            int articleId = Integer.parseInt(req.getParameter("article"));
            article= new ArticleService().getArticleById(articleId);
            req.setAttribute("article", article);
            RequestDispatcher rd=req.getRequestDispatcher("article.jsp");
            rd.forward(req, resp);
        } catch (SQLException | IOException e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }



    }
}
