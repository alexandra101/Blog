package controller.article;

import model.Article;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;
import service.UserService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {"", "/home"})
public class GetAllArticlesServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(GetAllArticlesServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        HttpSession session = req.getSession(false);
        if(session!=null&&session.getAttribute("email") != null){
            try {
                //set user displayed name
                String displayedName = new UserService().getUserLogin((String) session.getAttribute("email"));
                if (displayedName.equals(""))
                    req.setAttribute("displayedName", session.getAttribute("email"));
                else
                    req.setAttribute("displayedName", displayedName);
            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }
        }

        int page=1;
        int recordsPerPage = 3 ;

       //get all published articles and show them on main page
    ArrayList<Article> articles;
        try {

            if(req.getParameter("page") != null)
            page = Integer.parseInt(req.getParameter("page"));

            articles= (ArrayList<Article>) new ArticleService().getAllArticlesInRange(3,(page-1)*recordsPerPage);

            int noOfRecords =new ArticleService().getArticlesNumber();
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);

            req.setAttribute("noOfPages", noOfPages);
            req.setAttribute("currentPage", page);
            req.setAttribute("articles", articles);

            RequestDispatcher rd=req.getRequestDispatcher("index.jsp");
            rd.forward(req,resp);
        } catch (SQLException | IOException e ) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }

    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
        RequestDispatcher view = req.getRequestDispatcher("");
        view.forward(req, resp);
        } catch (IOException | ServletException e){
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }

    }
}
