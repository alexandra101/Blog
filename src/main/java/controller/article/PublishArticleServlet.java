package controller.article;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.ArticleService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

@WebServlet ("/publish")

public class PublishArticleServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(PublishArticleServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(false);
        if (session == null || session.getAttribute("email") == null) {

            try {
                resp.sendRedirect("notSignedIn.html");
            } catch (IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");

            }
        } else if (session.getAttribute("email") != null) {

            try {
                int articleId = Integer.parseInt(req.getParameter("article"));
                new ArticleService().publishArticle(articleId, true, new Timestamp(new Date().getTime()));
                RequestDispatcher rd = req.getRequestDispatcher("profile");
                rd.forward(req, resp);
            } catch (SQLException | IOException e) {
                log.error(e.getMessage());
                resp.sendRedirect("ErrorHandler");
            }
        }

    }
}
