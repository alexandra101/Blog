package controller.keyword;

import model.Keyword;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.KeywordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/update-keyword")
public class UpdateKeywordServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(UpdateKeywordServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int keywordId = Integer.parseInt(req.getParameter("keyword"));
        String keywordName = req.getParameter("keywordUpdatedName");
        try{

            new KeywordService().updateKeyword(Keyword.newBuilder().build(keywordId, keywordName));
            resp.sendRedirect("admin-page");
        } catch (SQLException e) {
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }
    }
}
