package controller.keyword;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.KeywordService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/delete-keyword")
public class DeleteKeywordServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(DeleteKeywordServlet.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       int keywordId = Integer.parseInt(req.getParameter("keyword"));

        try {
            req.setAttribute("keywordIsDeleted", new KeywordService().deleteKeyword(keywordId));
            RequestDispatcher rd = req.getRequestDispatcher("admin-page");
            rd.forward(req, resp);
        } catch (SQLException | IOException e) {
           log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }
    }
}
