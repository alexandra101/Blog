package controller.keyword;

import controller.article.UpdateArticleServlet;
import model.Keyword;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import service.KeywordService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/create-keyword")
public class CreateKeywordServlet extends HttpServlet {
    private static final Logger log = (Logger) LogManager.getLogger(CreateKeywordServlet.class);
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try{
            String keywordName = req.getParameter("keywordName");
            new KeywordService().createKeyword(Keyword.newBuilder().build(keywordName));
            resp.sendRedirect("admin-page");
        } catch (SQLException e){
            log.error(e.getMessage());
            resp.sendRedirect("ErrorHandler");
        }
    }
}
