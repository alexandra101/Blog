package dao;

import util.DBConnection;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleDAO {
    private String getRoleById = "SELECT role_name FROM roles WHERE id_role=?";



    public String getRoleById(int roleId) throws FileNotFoundException, SQLException {
        try (Connection connection = new DBConnection().createConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getRoleById)) {
                preparedStatement.setInt(1, roleId);

                try(ResultSet resultSet = preparedStatement.executeQuery() ){
                    resultSet.next();
                    return resultSet.getString(1);

                }
            }
        }


    }
}
