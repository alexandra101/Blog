package dao;

import model.User;
import org.springframework.security.crypto.bcrypt.BCrypt;
import util.DBConnection;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {

    private String createUser = "INSERT into users(user_login, user_password, user_role, user_email, user_isactive ) VALUES (?, ?, ?,?, ?)";
    private String authUser = "SELECT * FROM users where user_email=? AND user_isactive=true";
    private String getUserByEmail = "SELECT * FROM users WHERE user_email=?";
    private String getUserById = "SELECT * FROM users WHERE id_user=?";
    private String checkUserEmail="SELECT user_email FROM users WHERE user_email=?";
    private String getUserLogin = "SELECT user_login FROM users WHERE user_email=?";
    private String getAllUsers = "SELECT * FROM users ORDER BY user_login";
    private String banUser = "UPDATE users SET user_isactive=? where id_user=?";
    private String changeRole = "UPDATE users SET user_role=? WHERE id_user=?";

    private User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        return User.newBuilder().build(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getInt(4), resultSet.getString(5), resultSet.getBoolean(6));

    }


    public boolean registerUser(User user) throws SQLException, FileNotFoundException {
        String userLogin = user.getUserLogin();
        String userPassword = user.getUserPassword();
        int userRole = user.getUserRole();
        String userEmail = user.getUserEmail();

        try (Connection connection =  new DBConnection().createConnection()) {
            try (PreparedStatement preparedStatement1 = connection.prepareStatement(createUser)) {
                preparedStatement1.setString(1, userLogin);
                preparedStatement1.setString(2, userPassword);
                preparedStatement1.setInt(3, userRole);
                preparedStatement1.setString(4,userEmail);
                preparedStatement1.setBoolean(5,true);

    return preparedStatement1.executeUpdate()>0;
            }
        }

    }


    public boolean authenticateUser(User user) throws FileNotFoundException, SQLException {
        String userEmail = user.getUserEmail();
        String userPassword = user.getUserPassword();



        try (Connection connection =  new DBConnection().createConnection()) {
            try (PreparedStatement preparedStatement1 = connection.prepareStatement(authUser)) {
                preparedStatement1.setString(1, userEmail);

                try( ResultSet resultSet= preparedStatement1.executeQuery()){

                return resultSet.next()&&BCrypt.checkpw(userPassword, resultSet.getString("user_password"));}
            }
        }
    }


    public User getUserFromDB(String email) throws FileNotFoundException, SQLException {
        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(getUserByEmail)){
                preparedStatement.setString(1, email);
               try( ResultSet resultSet = preparedStatement.executeQuery()){

                   resultSet.next();
                return getUserFromResultSet(resultSet);}
            }

        }
    }
    public User getUserFromDB(int id) throws FileNotFoundException, SQLException {
        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(getUserById)){
                preparedStatement.setInt(1, id);
                try( ResultSet resultSet = preparedStatement.executeQuery()){

                    resultSet.next();
                    return getUserFromResultSet(resultSet);}
            }

        }
    }


    public boolean checkUserEmail(String email) throws FileNotFoundException, SQLException {
        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(checkUserEmail)){
                preparedStatement.setString(1, email);
                try( ResultSet resultSet = preparedStatement.executeQuery()){
                    return !resultSet.next();
                }
            }

        }
    }


   public String getUserLogin(String email) throws SQLException, FileNotFoundException {

        try(Connection connection = new DBConnection().createConnection()){
           try(PreparedStatement preparedStatement = connection.prepareStatement(getUserLogin)){
               preparedStatement.setString(1, email);
               try( ResultSet resultSet = preparedStatement.executeQuery()){

                   resultSet.next();
                   return   resultSet.getString(1);}
           }

       }
    }


    public List<User> getAllUsers() throws FileNotFoundException, SQLException {
        ArrayList<User> users = new ArrayList<>();
        try(Connection connection = new DBConnection().createConnection()){
            try (Statement statement = connection.createStatement()) {
                try(ResultSet resultSet = statement.executeQuery(getAllUsers)){
                    while (resultSet.next()){
                        users.add(getUserFromResultSet(resultSet));
                    }
                }


            }
        }
        return users;
    }


    public boolean banUser(boolean isActive, int userId) throws FileNotFoundException, SQLException {

        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(banUser)){
            preparedStatement.setBoolean(1,isActive);
            preparedStatement.setInt(2, userId);

            return preparedStatement.executeUpdate()>0;
            }
        }

    }

    public boolean changeRole(int role, int userId) throws FileNotFoundException, SQLException {
        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(changeRole)){
                preparedStatement.setInt(1,role);
                preparedStatement.setInt(2, userId);

                return preparedStatement.executeUpdate()>0;
            }
        }

    }
}
