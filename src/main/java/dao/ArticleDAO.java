package dao;

import model.Article;
import model.Keyword;
import model.User;
import service.KeywordService;
import util.DBConnection;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO {
    private String getAllPublishedArticles="SELECT * FROM articles where article_is_posted=true ORDER BY article_date_updated DESC";
    private String insertNewArticle="INSERT into articles(article_name, article_text, article_date_created, article_date_published, article_date_updated, article_image, article_is_posted, article_author_fk, keywords) VALUES (?, ?, ?, ?, ?, ?, ?, ?,?)";
    private String updateArticle = " UPDATE articles set article_name=?, article_text=?, article_date_updated=?,article_image=?, keywords=? WHERE id_article=?";
    private String updateArticleNoImage = "UPDATE articles set article_name=?, article_text=?, article_date_updated=?, keywords=? WHERE id_article=?";
    private String getUserArticles=" SELECT * FROM articles where article_author_fk=?  ORDER BY article_date_updated DESC";
    private String getArticleById=" SELECT * FROM articles WHERE id_article=?";
    private String publishArticle="SET CLIENT_ENCODING TO 'UTF8'; UPDATE articles SET article_is_posted=?, article_date_published=? WHERE id_article=?";
    private String getPostedArticlesInRange=" select * from articles WHERE article_is_posted=true ORDER BY article_date_updated DESC limit ? offset ? ";
   private String countPostedArticles=" SELECT COUNT(id_article) FROM articles WHERE article_is_posted=true";
   private String findArticleByTitle = " SELECT * FROM articles WHERE article_name ~* ? ORDER BY article_date_updated DESC";
    private String findArticleByKeyword = " SELECT * FROM articles WHERE keywords @> ?";
    private String deleteArticle = "DELETE FROM articles WHERE id_article=?";

    private String getAllUserArticlesInRange = "SELECT * FROM articles WHERE article_author_fk=?  ORDER BY article_date_updated DESC limit ? offset ?";
    private String countUserArticles = "SELECT COUNT(id_article) FROM articles WHERE article_author_fk=?";


    private Article getArticleFromResultSet(ResultSet resultSet) throws SQLException, FileNotFoundException {
        User user = new UserDAO().getUserFromDB(resultSet.getInt("article_author_fk"));
        Array kw =  resultSet.getArray("keywords");
        ArrayList<Keyword> articleKeywords1=new ArrayList<>();
        Integer [] keywords = (Integer[]) kw.getArray();

        for (int keyword : keywords) {
            articleKeywords1.add(new KeywordService().getKeywordsById(keyword));
        }

        return  Article.newBuilder().build(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getTimestamp(4), resultSet.getTimestamp(5), resultSet.getTimestamp(6),user, resultSet.getString(7) , resultSet.getBoolean(8), articleKeywords1);
    }



    private Array convertKeywordsToSQLArray(Connection connection, List<Keyword> keywords) throws SQLException {

        Object[] objects = new Object[keywords.size()];
        for (int i=0; i<keywords.size(); i++){
            objects[i]=keywords.get(i).getKeywordId();

        }
       return connection.createArrayOf("int", objects);
    }



    public List<Article> getAllArticles() throws FileNotFoundException, SQLException {
        ArrayList<Article> articles = new ArrayList<>();
        try (Connection connection =  new DBConnection().createConnection()) {
            try (Statement statement = connection.createStatement()) {
            try(ResultSet resultSet = statement.executeQuery(getAllPublishedArticles)){
                    while (resultSet.next()){
                        articles.add(getArticleFromResultSet(resultSet));
                    }
            }
            }
        }

   return articles; }


    public List<Article> getUserArticles(int userId) throws FileNotFoundException, SQLException {
        ArrayList<Article> articles = new ArrayList<>();
        try (Connection connection =  new DBConnection().createConnection()) {

                try(PreparedStatement preparedStatement = connection.prepareStatement(getUserArticles)){
                    preparedStatement.setInt(1,userId);
                    try(ResultSet resultSet = preparedStatement.executeQuery()) {
                        while (resultSet.next()){
                            articles.add(getArticleFromResultSet(resultSet));
                        }
                    }
            }

    }
        return articles;
    }


    public boolean createArticle(Article article) throws FileNotFoundException, SQLException {
        try(Connection connection = new DBConnection().createConnection()) {

                try(PreparedStatement preparedStatement = connection.prepareStatement(insertNewArticle)) {
                   preparedStatement.setString(1, article.getTitle());
                   preparedStatement.setString(2,article.getText());
                   preparedStatement.setTimestamp(3,article.getDateCreated());
                   preparedStatement.setTimestamp(4, article.getDatePublished());
                   preparedStatement.setTimestamp(5,article.getDateUpdated());
                   preparedStatement.setString(6,article.getImagePath());
                   preparedStatement.setBoolean(7,article.getPosted());
                   preparedStatement.setInt(8,article.getAuthor().getUserId());
                    preparedStatement.setArray(9,convertKeywordsToSQLArray(connection,article.getKeywords()));
                   return preparedStatement.execute();
                }
            }





    }



    public boolean updateArticle(Article article) throws FileNotFoundException, SQLException {

        try(Connection connection = new DBConnection().createConnection()) {
        try(PreparedStatement preparedStatement = connection.prepareStatement(updateArticle)) {
                preparedStatement.setString(1, article.getTitle());
                preparedStatement.setString(2,article.getText());
                preparedStatement.setTimestamp(3,article.getDateUpdated());
                preparedStatement.setString(4,article.getImagePath());
            preparedStatement.setArray(5,convertKeywordsToSQLArray(connection,article.getKeywords()));
                preparedStatement.setInt(6, article.getArticleId());

            return preparedStatement.executeUpdate() > 0;
        }
        }
    }



    public Article getArticleById(int articleId) throws FileNotFoundException, SQLException {
        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(getArticleById)){
                preparedStatement.setInt(1,articleId);
                try(ResultSet resultSet = preparedStatement.executeQuery()){
                    resultSet.next();
                    return getArticleFromResultSet(resultSet);
                }
            }
        }
    }

    public boolean publishArticle(int idArticle, boolean isPublished, Timestamp dateUpdated ) throws SQLException, FileNotFoundException {
        try(Connection connection = new DBConnection().createConnection()){
            try(PreparedStatement preparedStatement = connection.prepareStatement(publishArticle)){
                preparedStatement.setBoolean(1, isPublished);
                preparedStatement.setTimestamp(2, dateUpdated);
                preparedStatement.setInt(3,idArticle);
                return preparedStatement.executeUpdate()>0;
            }
        }

    }

    public boolean updateArticleNoImage(Article article) throws FileNotFoundException, SQLException {

        try(Connection connection = new DBConnection().createConnection()) {
            try(PreparedStatement preparedStatement = connection.prepareStatement(updateArticleNoImage)) {
                preparedStatement.setString(1, article.getTitle());
                preparedStatement.setString(2,article.getText());
                preparedStatement.setTimestamp(3,article.getDateUpdated());
                preparedStatement.setArray(4,convertKeywordsToSQLArray(connection,article.getKeywords()));
                preparedStatement.setInt(5, article.getArticleId());

                return preparedStatement.executeUpdate() > 0;




            }
        }
    }





    public List<Article> getAllArticlesInRange(int limit, int offset) throws FileNotFoundException, SQLException {

        ArrayList<Article> articles = new ArrayList<>();
        try (Connection connection =  new DBConnection().createConnection()) {
            try(PreparedStatement preparedStatement = connection.prepareStatement(getPostedArticlesInRange)){
                preparedStatement.setInt(1,limit);
                preparedStatement.setInt(2,offset);

                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()){
                        articles.add(getArticleFromResultSet(resultSet));
                    }
                }
            }

    }

    return articles;}



    public int getArticlesNumber() throws SQLException, FileNotFoundException {
        try (Connection connection =  new DBConnection().createConnection()) {
            try (Statement statement = connection.createStatement()) {
                try(ResultSet resultSet = statement.executeQuery(countPostedArticles)){

                resultSet.next();
                return resultSet.getInt(1);
                }
            }

    }

    }



    public List<Article> findArticleByTitle(String title) throws FileNotFoundException, SQLException {
        ArrayList<Article> articles = new ArrayList<>();
        StringBuilder searchExpr = new StringBuilder();
        String[] splitTitle = title.split(" ");
        for (String string: splitTitle) {
            searchExpr.append("(?=.*").append(string).append(")");
        }
        try (Connection connection =  new DBConnection().createConnection()) {

            try(PreparedStatement preparedStatement = connection.prepareStatement(findArticleByTitle)){
                preparedStatement.setString(1, String.valueOf(searchExpr));
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()){
                        articles.add(getArticleFromResultSet(resultSet));
                    }
                }
            }

        }
        return articles;
    }


    public List<Article> findArticleByKeyword(int keyword) throws FileNotFoundException, SQLException {
        ArrayList<Article> articles = new ArrayList<>();


               try (Connection connection =  new DBConnection().createConnection()) {
                   Array keywords =connection.createArrayOf("int", new Object[]{keyword});
                   try(PreparedStatement preparedStatement = connection.prepareStatement(findArticleByKeyword)){
                preparedStatement.setArray(1,keywords);
                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()){
                        articles.add(getArticleFromResultSet(resultSet));
                    }
                }
            }

        }
        return articles;
    }



    public boolean deleteArticle(int articleId) throws FileNotFoundException, SQLException {
        try (Connection connection = new DBConnection().createConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(deleteArticle)) {
                preparedStatement.setInt(1, articleId);
                return preparedStatement.executeUpdate() > 0;
            }

        }
    }


    public List<Article> getAllUserArticlesInRange(int authorId, int limit, int offset) throws FileNotFoundException, SQLException {

        ArrayList<Article> articles = new ArrayList<>();
        try (Connection connection =  new DBConnection().createConnection()) {
            try(PreparedStatement preparedStatement = connection.prepareStatement(getAllUserArticlesInRange)){
                preparedStatement.setInt(1,authorId);
                preparedStatement.setInt(2,limit);
                preparedStatement.setInt(3,offset);

                try(ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()){
                        articles.add(getArticleFromResultSet(resultSet));
                    }
                }
            }

        }

        return articles;}



    public int getUserArticlesNumber(int authorId) throws SQLException, FileNotFoundException {
        try (Connection connection =  new DBConnection().createConnection()) {
            try(PreparedStatement preparedStatement = connection.prepareStatement(countUserArticles)){
                preparedStatement.setInt(1,authorId);

                try(ResultSet resultSet = preparedStatement.executeQuery()){
                    resultSet.next();
                return resultSet.getInt(1);}
                }
            }

        }

    }








