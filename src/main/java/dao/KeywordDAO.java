package dao;

import model.Keyword;
import util.DBConnection;

import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;

public class KeywordDAO {

    private String getAllKeywords = "SELECT * FROM keywords";
    private String insertNewKeyword = "INSERT INTO keywords(keyword_name)  VALUES (?)";
    private String updateKeyword = "UPDATE keywords SET keyword_name=? WHERE id_keyword=?";
    private String getKeywordsById = "SELECT * FROM keywords WHERE id_keyword=?";
    private String deleteKeyword = "DELETE FROM keywords WHERE id_keyword=?";

    private Keyword getKeywordFromResultSet(ResultSet resultSet) throws SQLException {
        return Keyword.newBuilder().build(resultSet.getInt(1), resultSet.getString(2));

    }




    public ArrayList<Keyword> getAllKeywords() throws FileNotFoundException, SQLException {
        ArrayList<Keyword> keywords = new ArrayList<>();
        try (Connection connection = new DBConnection().createConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(getAllKeywords)) {
                    while (resultSet.next()) {
                        keywords.add(getKeywordFromResultSet(resultSet));
                    }
                }


            }


        }
        return keywords;
    }

    public boolean createKeyword(Keyword keyword) throws FileNotFoundException, SQLException {
        try (Connection connection = new DBConnection().createConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(insertNewKeyword)) {
                preparedStatement.setString(1, keyword.getKeywordName());

                return preparedStatement.execute();
            }

        }
    }

    public boolean updateKeyword(Keyword keyword) throws FileNotFoundException, SQLException {
        try (Connection connection = new DBConnection().createConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(updateKeyword)) {
                preparedStatement.setString(1, keyword.getKeywordName());
                preparedStatement.setInt(2, keyword.getKeywordId());
                return preparedStatement.executeUpdate() > 0;
            }

        }
    }


    public Keyword getKeywordsById(int keywordId) throws FileNotFoundException, SQLException {
        try (Connection connection = new DBConnection().createConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(getKeywordsById)) {
                preparedStatement.setInt(1, keywordId);

               try(ResultSet resultSet = preparedStatement.executeQuery() ){
                   resultSet.next();
                   return getKeywordFromResultSet(resultSet);

               }
            }
        }


    }

    public boolean deleteKeyword(int keywordId) throws FileNotFoundException, SQLException {
        try (Connection connection = new DBConnection().createConnection()) {

            try (PreparedStatement preparedStatement = connection.prepareStatement(deleteKeyword)) {
                preparedStatement.setInt(1, keywordId);
                return preparedStatement.executeUpdate() > 0;
            }

        }
    }
}

