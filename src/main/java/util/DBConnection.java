package util;

import controller.user.RegisterServlet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;


public class DBConnection {
    private String url;
  private    String username;
   private String pass;
    private static final Logger log = (Logger) LogManager.getLogger(DBConnection.class);
    public DBConnection(){


    }
    public  Connection createConnection() throws FileNotFoundException {

        FileInputStream fileInputStream;
        Properties prop = new Properties();

        try {

            prop.load(getClass().getResourceAsStream("/db.properties"));
           url = prop.getProperty("db.host");
           username = prop.getProperty("db.login");
           pass = prop.getProperty("db.password");


        } catch (IOException e) {
         log.error(e.getMessage());
        }



        Connection con = null;


        try
        {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e)
        {

            log.error(e.getMessage());
        }

        try
        {


            con = DriverManager.getConnection(url, username, pass); //attempting to connect to MySQL database

        }
        catch (Exception e)
        {
            log.error(e.getMessage());
        }

        return con;
    }
}

