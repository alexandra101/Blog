package model;

import service.ValidationService;

import javax.validation.constraints.NotEmpty;


public class Keyword {

    private int keywordId;

    @NotEmpty
    private String keywordName;

    private Keyword(){
        //
    }

     public int getKeywordId() {
        return keywordId;
    }


    public String getKeywordName() {
        return keywordName;
    }


    public static Builder newBuilder() {
        return new Keyword().new Builder();
    }


    //builder


public class Builder{
   private Builder(){

   }

    public Builder setKeywordId(int keywordId) {
        Keyword.this.keywordId = keywordId;
        return this;
    }
    public Builder setKeywordName(String keywordName) {
        Keyword.this.keywordName = keywordName;
        return this;
    }

    public Keyword build(){
       if (ValidationService.validate(Keyword.this)){
       return Keyword.this;} else return null;

    }

    public Keyword build(int keywordId, String keywordName){
        Keyword.this.keywordId = keywordId;
        Keyword.this.keywordName = keywordName;
        if (ValidationService.validate(Keyword.this)){
            return Keyword.this;} else return null;
    }

    public Keyword build(String keywordName){
        Keyword.this.keywordName = keywordName;
        if (ValidationService.validate(Keyword.this)){
            return Keyword.this;} else return null;
    }


}
}
