package model;

import service.ValidationService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Article {

    private int articleId;

    @NotEmpty
    @Size(min = 3, max = 128, message = "Title must have 3-128 characters")
    private String title;

    private String text;
    @PastOrPresent
    private Timestamp dateCreated;
    @PastOrPresent
    private Timestamp datePublished;
    @PastOrPresent
    private Timestamp dateUpdated;

    @NotNull
    private User author;
    private String imagePath;
    @NotNull
    private boolean posted;
    @NotNull
    private ArrayList<Keyword> keywords;

    private Article(){

    }

   public int getArticleId() {
        return articleId;
    }
   public String getTitle() {
        return title;
    }
    public String getText() {
        return text;
    }
    public Timestamp getDateCreated() {
        return dateCreated;
    }
    public Timestamp getDatePublished() {
        return datePublished;
    }
    public Timestamp getDateUpdated() {
        return dateUpdated;
    }
    public User getAuthor() {
        return author;
    }
    public String getImagePath() {
        return imagePath;
    }
    public boolean getPosted() {
        return posted;
    }


    public List<Keyword> getKeywords() {
        return keywords;
    }


    public static Builder newBuilder() {
        return new Article().new Builder();
    }



    //----------------

    public class Builder{
        private Builder(){
        }

        public Builder setArticleId(int articleId){
            Article.this.articleId=articleId;
            return this;
        }

        public Builder setArticleTitle(String title){
            Article.this.title=title;
            return this;
        }

        public Builder setText(String text){
            Article.this.text=text;
            return this;
        }

        public Builder setDateCreated(Timestamp dateCreated) {
            Article.this.dateCreated = dateCreated;
            return this;
        }

        public Builder setDatePublished(Timestamp datePublished) {
            Article.this.datePublished = datePublished;
            return this;
        }

        public Builder setDateUpdated(Timestamp dateUpdated) {
            Article.this.dateUpdated = dateUpdated;
            return this;
        }

        public Builder setAuthor(User author) {
            Article.this.author = author;
            return this;
        }

        public Builder setImagePath(String imagePath) {
            Article.this.imagePath = imagePath;
            return this;
        }

        public Builder setPosted(boolean posted) {
            Article.this.posted = posted;
            return this;
        }

        public Builder setKeywords(List<Keyword> keywords) {
            Article.this.keywords = (ArrayList<Keyword>) keywords;
            return this;
        }



        public Article build(String title, String text, Timestamp dateCreated, Timestamp datePublished, Timestamp dateUpdated, User author, String imagePath, boolean isPosted, List<Keyword> keywords){
            Article.this.title=title;
            Article.this.text=text;
            Article.this.dateCreated = dateCreated;
            Article.this.datePublished = datePublished;
            Article.this.dateUpdated = dateUpdated;
            Article.this.author = author;
            Article.this.imagePath = imagePath;
            Article.this.posted = isPosted;
            Article.this.keywords = (ArrayList<Keyword>) keywords;

            if (ValidationService.validate(Article.this)){
           return Article.this;} else return null;
        }


        public Article build(int articleId, String title, String text, Timestamp dateCreated, Timestamp datePublished, Timestamp dateUpdated, User author, String imagePath, boolean isPosted, List<Keyword> keywords){
            Article.this.articleId=articleId;
            Article.this.title=title;
            Article.this.text=text;
            Article.this.dateCreated = dateCreated;
            Article.this.datePublished = datePublished;
            Article.this.dateUpdated = dateUpdated;
            Article.this.author = author;
            Article.this.imagePath = imagePath;
            Article.this.posted = isPosted;
            Article.this.keywords = (ArrayList<Keyword>) keywords;

            if (ValidationService.validate(Article.this)){
                return Article.this;} else return null;
        }

        public Article build(){
            if (ValidationService.validate(Article.this)){
                return Article.this;} else return null;
        }


    }

}


