package model;

import service.ValidationService;

import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.el.ExpressionFactory;

public class User {



    private int userId;


    @Size(min=3, max=120)
    private  String userLogin;

    @Size(min=8)
    @NotEmpty(message = "Password cannot be empty")
    @Pattern(regexp="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-/]).{8,100}$", message="Password Must contain at least 8 characters, one lowercase, one uppercase and one special symbol")
    private String userPassword;

    @NotNull(message = "Role cannot be null")
  @Min(1)
    @Max(3)
    private int userRole;

    @NotEmpty(message = "Email cannot be empty")
    @Email
    @Pattern(regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}$")
    private String userEmail;


    @NotNull
    private boolean isActive;

    private User(){

    }


    public int getUserId() {
        return userId;
    }



    public String getUserLogin() {
        return userLogin;
    }



    public String getUserPassword() {
        return userPassword;
    }



    public int getUserRole() {
        return userRole;
    }



    public String getUserEmail() {
        return userEmail;
    }



    public static Builder newBuilder() {
        return new User().new Builder();
    }

    public boolean getIsActive() {
        return isActive;
    }


    public class Builder{
        private Builder(){

        }

        public Builder setUserId(int userId) {
            User.this.userId = userId;
            return this;
        }

        public Builder setUserLogin(String userLogin) {
            User.this.userLogin = userLogin;
            return this;
        }

        public Builder setUserPassword(String userPassword) {
            User.this.userPassword = userPassword;
            return this;
        }

        public Builder setUserRole(int userRole) {
            User.this.userRole = userRole;
            return this;
        }

        public Builder setUserEmail(String userEmail) {
            User.this.userEmail = userEmail;
            return this;
        }


        public Builder setUserIsActive(boolean isActive){
            User.this.isActive = isActive;
            return this;
        }

        public User build(){

            if (ValidationService.validate(User.this))
            return User.this;

            else return null;
        }

        public User build(String userLogin, String userPassword, int userRole, String userEmail, boolean isActive){
            User.this.userLogin=userLogin;
            User.this.userPassword=userPassword;
            User.this.userRole=userRole;
            User.this.userEmail=userEmail;
            User.this.isActive=isActive;

            if (ValidationService.validate(User.this))
                return User.this;

            else return null;
        }


        public User build(int userId, String userLogin, String userPassword, int userRole, String userEmail, boolean isActive){
            User.this.userId=userId;
            User.this.userLogin=userLogin;
            User.this.userPassword=userPassword;
            User.this.userRole=userRole;
            User.this.userEmail=userEmail;
            User.this.isActive=isActive;
            if (ValidationService.validate(User.this))
                return User.this;

            else return null;
        }
    }


}
