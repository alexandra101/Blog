package service;

import dao.RoleDAO;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public class RoleService {
    public String getRoleById(int roleId) throws FileNotFoundException, SQLException {
        return new RoleDAO().getRoleById(roleId);
    }
}
