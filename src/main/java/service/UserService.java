package service;

import dao.UserDAO;
import model.User;
import org.springframework.security.crypto.bcrypt.BCrypt;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public class UserService {


    private String hashPass(String password){
       return BCrypt.hashpw(password, BCrypt.gensalt());
    }


    public boolean registerUser(User user) throws SQLException, FileNotFoundException {
        String hashedPass=hashPass(user.getUserPassword());
        User userToRegister = User.newBuilder().build(user.getUserLogin(), hashedPass, user.getUserRole(), user.getUserEmail(), user.getIsActive());
        return checkUserEmail(userToRegister.getUserEmail())&&(new UserDAO().registerUser(userToRegister));
    }

    public boolean authenticateUser(User user) throws FileNotFoundException, SQLException {


        return new UserDAO().authenticateUser(user);
    }

    public User getUserFromDB(String email) throws FileNotFoundException, SQLException {
        return new UserDAO().getUserFromDB(email);
    }
    public User getUserFromDB(int id) throws FileNotFoundException, SQLException {
        return new UserDAO().getUserFromDB(id);
    }

    private boolean checkUserEmail(String email) throws FileNotFoundException, SQLException {
      return new UserDAO().checkUserEmail(email);
    }


    public String getUserLogin(String email) throws SQLException, FileNotFoundException {
        return new UserDAO().getUserLogin(email);
    }


    public List<User> getAllUsers() throws FileNotFoundException, SQLException {
        return new UserDAO().getAllUsers();
    }

    public boolean banUser(boolean isActive, int userId) throws FileNotFoundException, SQLException {
        return new UserDAO().banUser(isActive, userId);
    }

    public boolean changeRole(int role, int userId) throws FileNotFoundException, SQLException {
        return new UserDAO().changeRole(role, userId);
    }
}
