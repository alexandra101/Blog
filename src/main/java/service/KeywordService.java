package service;

import dao.KeywordDAO;
import model.Keyword;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

public class KeywordService {
    public ArrayList<Keyword> getAllKeywords() throws FileNotFoundException, SQLException {
        return new KeywordDAO().getAllKeywords();
    }



    public boolean createKeyword(Keyword keyword) throws FileNotFoundException, SQLException {
        return new KeywordDAO().createKeyword(keyword);
    }


    public boolean updateKeyword(Keyword keyword) throws FileNotFoundException, SQLException {

        return new KeywordDAO().updateKeyword(keyword);
    }


    public Keyword getKeywordsById(int keywordId) throws FileNotFoundException, SQLException {
     return    new KeywordDAO().getKeywordsById(keywordId);
    }

    public boolean deleteKeyword(int keywordId) throws FileNotFoundException, SQLException {
        boolean isUsed = new ArticleService().findArticleByKeyword(keywordId).isEmpty();
        if (isUsed){
        return new KeywordDAO().deleteKeyword(keywordId);}
        else
            return false;
    }
}
