package service;

import dao.ArticleDAO;
import model.Article;
import org.apache.commons.io.FilenameUtils;

import javax.servlet.http.Part;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

public class ArticleService {

    public List<Article> getAllArticles() throws FileNotFoundException, SQLException {
        return new ArticleDAO().getAllArticles();
    }


    public List<Article> getUserArticles(int userId) throws FileNotFoundException, SQLException {
        return new ArticleDAO().getUserArticles(userId);
    }


    public boolean createArticle(Article article) throws FileNotFoundException, SQLException {
        return new ArticleDAO().createArticle(article);
    }

    public boolean updateArticle(Article article) throws FileNotFoundException, SQLException {

        return new ArticleDAO().updateArticle(article);
    }

    public Article getArticleById(int articleId) throws FileNotFoundException, SQLException {
        return new ArticleDAO().getArticleById(articleId);
    }

    public boolean publishArticle(int idArticle, boolean isPublished, Timestamp dateUpdated ) throws SQLException, FileNotFoundException {
        return new ArticleDAO().publishArticle(idArticle, isPublished, dateUpdated);
    }


    public boolean createOrUpdateArticle(Article article, boolean create, boolean deleteImage) throws FileNotFoundException, SQLException {
        if (create) return createArticle(article);

        else
        if (article.getImagePath()!=null||deleteImage) {
            return updateArticle(article);

        } else
            return updateArticleNoImage(article);
    }


   private boolean updateArticleNoImage(Article article) throws FileNotFoundException, SQLException {
    return new ArticleDAO().updateArticleNoImage(article);
    }


    public List<Article> getAllArticlesInRange(int limit, int offset) throws FileNotFoundException, SQLException {
        return new ArticleDAO().getAllArticlesInRange(limit, offset);
    }



    public String saveImage(Part part, String appPath) throws IOException {

        File fileSaveDir = new File(appPath+"uploadFiles");
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }

        String fileName = part.getSubmittedFileName();
        if(fileName.contains(".jpg")||fileName.contains(".png")||fileName.contains(".gif")){
        fileName = new File(fileName).getName();
        fileName="uploadFiles"+File.separator +new Random().nextInt()+"_"+fileName;
        part.write(appPath+fileName);
      return fileName;} else return null;
    }


    public List<Article> findArticleByTitle(String title) throws FileNotFoundException, SQLException {
        return new ArticleDAO().findArticleByTitle(title);
    }

    public List<Article> findArticleByKeyword(int keyword) throws FileNotFoundException, SQLException {
        return new ArticleDAO().findArticleByKeyword(keyword);
    }

    public boolean deleteArticle(int articleId) throws FileNotFoundException, SQLException {
        return new ArticleDAO().deleteArticle(articleId);
    }

    public int getArticlesNumber() throws SQLException, FileNotFoundException {
        return new ArticleDAO().getArticlesNumber();
    }


    public List<Article> getAllUserArticlesInRange(int authorId, int limit, int offset) throws FileNotFoundException, SQLException {
        return new ArticleDAO().getAllUserArticlesInRange(authorId, limit, offset);
    }

    public int getUserArticlesNumber(int authorId) throws SQLException, FileNotFoundException {
        return new ArticleDAO().getUserArticlesNumber(authorId);
    }
}