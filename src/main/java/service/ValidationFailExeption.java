package service;

public class ValidationFailExeption extends Exception {

    public ValidationFailExeption(String message){
        super(message);
    }
}
