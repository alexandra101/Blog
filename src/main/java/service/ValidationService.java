package service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

import javax.validation.*;
import javax.validation.ValidatorFactory;
import javax.validation.Validator;
import java.util.Set;

public class ValidationService {
    private static final Logger log = (Logger) LogManager.getLogger(ValidationService.class);

    public static boolean validate(Object object) {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object);

      log.info(object);
        log.info(String.format("Errors number: %d",
                constraintViolations.size()));

        StringBuilder message = new StringBuilder();
        for (ConstraintViolation<Object> cv : constraintViolations){
            message.append(String.format("Error! property: [%s], value: [%s], message: [%s]",
                    cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));

            log.info(String.format(
                    "Error! property: [%s], value: [%s], message: [%s]",
                    cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));}

        return constraintViolations.isEmpty();
    }
}
