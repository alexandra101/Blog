<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 10.07.2018
  Time: 0:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<title>Oops</title>
<link rel="stylesheet" type="text/css" href="./css/style.css">
<link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
<div class="container-fluid w-75 center-block text-center">
    <img src="./images/oops.png" class="h-50">
    <p>Sorry, something went wrong!</p>
    <div class="text-left alert-danger">
        <p class="text-danger "><c:out value="${error}"/> </p>
    </div>
    <a href="home" class="btn btn-success">Try to go back to Home Page</a>
</div>
</body>
</html>
