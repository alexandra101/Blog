<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 29.06.2018
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
    </head>

    <body>
    <div class="text-center">
    <div class="card w-25 center-block border-0" >
    <div class="card-img">    <img  src="images/logo.png"></div>
    <div class="card-header">Login</div>
<form action="login" method="post" class="form-signin">

        <input name="userEmail" type="text" placeholder="Enter your e-mail" required class="form-control">


        <input name="password" type="password" placeholder="Password" required class="form-control">

    <c:if test="${failedAuth!=null}">
    <p class="alert-danger">Wrong e-mail or password!</p>
    </c:if>


    <input name="submit" type="submit" class="btn btn-primary btn-block mt-2" value="Sign in">
    <a href="home" class="btn btn-secondary mt-2 w-100">Cancel</a>



</div>

<c:if test="${sessionScope.email!=null}">
    <div class="modal-seen">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Success!</h5>
                                    </div>
                <div class="modal-body">
                 You have successfully signed in!
                </div>
                <div class="modal-footer ">
                    <a class="btn btn-primary center-block"  href="home">Go to Home Page</a>
                </div>
            </div>
        </div>
    </div>
    </div>
</c:if>


</body>
</html>
