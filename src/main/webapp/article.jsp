<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 02.07.2018
  Time: 15:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Article</title>


    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
    <body>
    <div class="container-fluid">

        <c:import url="navbar.jsp"/>

<div class="container mt-5 p-2">
        <div class="card  text-center">

            <div class="card-header">
                <p class="text-uppercase font-weight-bold"><a class="card-link text-dark" href="article?article=${article.articleId}">${article.title}</a>
                    <c:forEach items="${article.keywords}" var="keyword">
                    <span class="badge badge-dark">${keyword.keywordName}</span>
                    </c:forEach>
                <p class="text-secondary">by ${article.author.userLogin}</p>
                <p class="text-secondary">Published on: ${article.datePublished}</p>
                <p class="text-secondary">Updated on: ${article.dateUpdated}</p>

            </div>
            <div class="card-body">

                <div class="card-img mb-2">
                    <c:if test="${article.imagePath!=null}">
                        <img class="img-fluid h-50" src="${article.imagePath}">
                    </c:if>

                </div>

                <div class="card-text">
                    <p class="text-justify">${article.text}</p>
                </div>

            </div>

        </div>

</div>
    </div>
</body>
</html>
