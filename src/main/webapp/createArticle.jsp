<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 02.07.2018
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create article</title>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap-select.css">
    <script src="./js/bootstrap-select.bundle.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
    <body>

    <c:import url="navbar.jsp"/>
    <div class="container-fluid text-center mt-5">
        <h2>Create article</h2>

<div class="container w-75 center-block text-justify">


<form action="create-article" method="post"  enctype="multipart/form-data">


<div class="form-group">
    <label for="articleTitle"> Title     </label>
        <input class="form-control w-100" name="title" type="text" id="articleTitle" placeholder="Enter article title" required pattern=".*{3,128}">
</div>


    <div class="form-group">
    <label for="articleText">Text     </label>
 <textarea class="form-control" name="text" id="articleText"  ></textarea>
    </div>


    <div class="form-group">
    <label for="articleImage">Select image    </label>
    <input class="form-control" name="image" type="file" id="articleImage" accept="image/jpeg,image/png,image/gif">
    </div>

    <div class="form-group">
        <label for="keywords">Select keywords</label>
       <select  required id="keywords" class="selectpicker" multiple name="keywords">
            <c:forEach items="${keywords}" var="keyword">
           <option style="padding: 0" value="${keyword.keywordId}">${keyword.keywordName}</option>
            </c:forEach>
        </select>

        <c:out value="${keyword.keywordName}"/>
    </div>

    <div class="form-check">
        <input name="isPosted" type="checkbox" value="true" id="articlePosted">
        <label for="articlePosted">Post article</label>
    </div>



    <input name="submit" class="btn btn-success" type="submit" value="Create" id="articleSubmit">

 <a href="profile" class="btn btn-secondary">Cancel</a>

</form>
        </div>

    </div>
</body>
</html>
