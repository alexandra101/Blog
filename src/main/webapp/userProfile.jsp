<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 03.07.2018
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Profile</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>


    <body>


    <div class="container-fluid text-center">
        <c:import url="navbar.jsp"/>

        <div class="panel panel-success mt-5 text-center">

        <c:if test="${sessionScope.email!=null}">
       <div class="panel-heading"> <p class="alert-success p-3 text-uppercase font-weight-bold">Welcome, ${displayedName}!</p></div>



<div class="panel-body">
        <a href="create-article" class="btn btn-success">Create Article</a>
    <c:if test="${sessionScope.role==1}"><a href="admin-page" class="btn btn-primary">Admin Page</a></c:if>
</div>
        </div>



        <div class="container mt-5 p-2 w-75">
            <h2 >Your articles</h2>
            <c:forEach items="${articles}" var="article">
            <div class="card mt-3 text-center">

                <div class="card-header">

                    <div class="btn-group">
                        <a class="btn btn-sm btn-primary " href=update-article?article=${article.articleId}> Edit</a>

                        <c:if test="${article.posted}">
                            <button class="btn btn-sm btn-warning " data-toggle="modal" data-target="#modalUnPublish${article.articleId}">Unpublish</button>
                        </c:if>
                        <c:if test="${!article.posted}">
                            <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#modalPublish${article.articleId}">Publish</button>
                        </c:if>

                        <a class="btn btn-sm btn-danger" href="delete-article?article=${article.articleId}">Delete</a>

                    </div>
                    <br>
                    <span class="text-uppercase font-weight-bold"><a class="card-link text-dark" href="article?article=${article.articleId}">${article.title}</a></span>
                    <br>
                        <c:forEach items="${article.keywords}" var="keyword">
                        <span class="badge badge-dark">${keyword.keywordName}</span>
                        </c:forEach>

             <div>
                    <span class="text-secondary"> by ${article.author.userLogin} </span>

                        <c:if test="${article.posted}">

                    <div class="alert-success">
                        <span>Published</span>

<br>
                    <!--Modal publish/unpublish-->

                    <div class="modal fade" id="modalUnPublish${article.articleId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-center ">
                                    Are you sure you want to unpublish this article?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                                    <a href=unpublish?article=${article.articleId}  class="btn btn-primary text-uppercase">yes</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            <p class="text-secondary">Published on: ${article.datePublished}</p>

                        </c:if>

                    <c:if test="${!article.posted}">

                        <div class="alert-danger">
                            <span>Not published</span>

                            <br>
                            <!--Modal-->

                            <div class="modal fade" id="modalPublish${article.articleId}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="NO">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center ">
                                            Are you sure you want to publish this article?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <a href=publish?article=${article.articleId}  class="btn btn-primary text-uppercase">yes</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </c:if>
                    <p class="text-secondary">Updated on: ${article.dateUpdated}</p>

                </div>
        </div>
                <div class="card-body">
                    <div class="card-img">
                    <c:if test="${article.imagePath!=null}">
                        <img class="img-fluid" src="${article.imagePath}">
                    </c:if>

                    </div>
                    <p class="card-text text-justify text-truncate">${article.text}</p>


                </div>
                </div>
            </c:forEach>

        </div>

</div>
    </c:if>



    <ul class="pagination justify-content-center" style="margin:20px 0">
    <c:if test="${currentPage != 1}">
        <li class="page-item"><a class="page-link" href="profile?page=${currentPage - 1}">Previous</a></li>
    </c:if>

    <c:forEach begin="1" end="${noOfPages}" var="i">
        <c:choose>
            <c:when test="${currentPage eq i}">
                <li class="page-item active"><a class="page-link" href="profile?page=${i}"> ${i}</a></li>
            </c:when>
            <c:otherwise>
                <li class="page-item"><a class="page-link" href="profile?page=${i}">${i}</a></li>
            </c:otherwise>
        </c:choose>
    </c:forEach>


    <c:if test="${currentPage lt noOfPages}">
        <li class="page-item"><a class="page-link" href="profile?page=${currentPage + 1}">Next</a></li>
    </c:if>
    </ul>

<c:import url="footer.jsp"/>

</body>
</html>
