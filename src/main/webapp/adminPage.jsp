<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 13.07.2018
  Time: 1:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Page</title>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
</head>
<body>

<div class="container-fluid text-center">
    <c:import url="navbar.jsp"/>



    <div class="row mt-5">
    <div class="card col-md-6">
        <div class="card-header"><h2>User List</h2></div>
        <div class="panel mt-5 bg-light">
            <a href="register" class="btn btn-primary">Add user</a>
        </div>
        <div class="card-body">

            <div class="table-responsive table-striped table-hover h-75">
                <table class="table">
                    <thead>
                    <tr class="info">
                        <th>#</th>
                        <th>Displayed name</th>
                        <th>e-mail</th>
                        <th>Role</th>
                        <th>Is Active</th>
                    </tr>

                    </thead>
                    <tbody>
                    <c:set var="userIndex" value="1"/>
                   <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${userIndex}</td>
                        <td>${user.userLogin}</td>
                        <td>${user.userEmail}</td>
                     <td>



                            <c:if test="${user.userRole==1}"> ADMIN</c:if>
                             <c:if test="${user.userRole==2}">USER </c:if>

                     </td>

                        <td>

                        <c:if test="${sessionScope.email!=user.userEmail}">
                        <c:if test="${user.isActive==true}">
                            <a href="ban-user?user=${user.userId}"><img class="h-50" src="images/isActive.png"></a>
                        </c:if>


                        <c:if test="${user.isActive==false}">
                        <a href="unban-user?user=${user.userId}"><img src="images/notActive.png" class="h-50"></a>
                       </c:if>
                        </c:if>
                        </td>
                    </tr>
                       <c:set var="userIndex" value="${userIndex+1}"/>
                   </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

        <div class="card col-md-6" >

            <div class="card-header"><h2>Keywords List</h2></div>
            <div class="panel mt-5 bg-light">
                <form  action="create-keyword" method="post">
                    <input type="text" class="form-control" name="keywordName" placeholder="Enter keyword value">
                <input type="submit" class="btn-primary form-control" value="Create keyword">
                </form>
            </div>
            <div class="card-body">

                <div class="table-responsive table-striped table-hover h-50">
                    <c:set var="keywordIndex" value="1"/>
                    <table class="table">
                        <thead>
                        <tr class="info">
                            <th>#</th>
                            <th>Keyword Value</th>
                        </tr>
                        </thead>
                        <tbody>



                        <c:forEach items="${keywords}" var="keyword">
                            <tr>

                                <td>${keywordIndex}</td>
                                <form action="update-keyword?keyword=${keyword.keywordId}" method="post">
                                <td><input type="text" placeholder="Keyword value" class="form-control"  value="${keyword.keywordName}" name="keywordUpdatedName"></td>
                                <td> <input type="submit" class="btn btn-sm btn-success" value="Edit"></td>
                                <td> <a class="btn btn-sm btn-danger" href="delete-keyword?keyword=${keyword.keywordId}">Delete</a></td>
                                </form>
                            </tr>

                            <c:set var="keywordIndex" value="${keywordIndex+1}"/>

                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


</div>



</div>



<c:if test="${keywordIsDeleted==true}">
    <div class="modal-seen" id="modalKeywordIsDeleted" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Success!</h5>
                </div>
                <div class="modal-body">
                    Keyword is successfully deleted!
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-secondary" onclick = "$('#modalKeywordIsDeleted').hide()">Close</button>
                </div>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${keywordIsDeleted==false}">
    <div class="modal-seen" id="modalKeywordIsNotDeleted" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle2">Not deleted</h5>
                </div>
                <div class="modal-body">
                    Keyword is not deleted since there are articles that use it!
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-secondary" onclick = "$('#modalKeywordIsNotDeleted').hide()">Close</button>
                </div>
            </div>
        </div>
    </div>
</c:if>
</body>
</html>
