<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 29.06.2018
  Time: 13:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>


    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>



</head>


<body>
<div class="container-fluid">

    <c:import url="navbar.jsp"/>


    <div class="container text-center">
        <div class="card w-25 center-block border-0">
            <div class="card-img"><img src="./images/logo.png"></div>
            <div class="card-header">Register</div>
            <form action="register" method="post" class="form-signin" id="registerForm" onsubmit="return validateForm()">

                <input id="username" class="form-control mt-2" placeholder="Your displayed name" autofocus required name="userLogin" type="text" pattern="^(?=.{3,20}$"
                title="Your displayed name must be 3-20 characters long ">


                <div class="input-group">

                <input class="form-control mt-2" placeholder="Password" autofocus required name="password" type="password" id="psw"
                       title="Password must contain 8-15 characters, one lowercase, one uppercase, one number and one special symbol"
                       pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,15}$"  >
                <button class="btn btn-sm input-group-append mt-2 btn-light" type="button" onclick="showPassword()"><span id="#showPasswordSpan" class="fa fa-eye"></span></button>
                </div>
                <input class="form-control mt-2" placeholder="Confirm password" required name="password_confirm" type="password" onchange="checkPasswordMatch()" id="pswConf">
                <div id="divCheckPasswordMatch" class="alert-danger"></div>
                <input  class="form-control mt-2" placeholder="e-mail" autofocus required name="email" type="email" id="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">


                <c:if test="${sessionScope.role!=null&&sessionScope.role==1}">
                    <div class="form-check">
                    <label>
                    <input type="checkbox" name="makeAdmin" class="form-check-input" value="true">
                        Make admin
                    </label>
                </div>
                </c:if>
                <input class="btn btn-primary mt-2" value="Register!" type="submit">
            </form>
        </div>
    </div>
</div>

<c:if test="${success==1}">
        <div class="modal-seen modal-dialog modal-dialog-centered" id="modalRegistered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Success!</h5>
                </div>
                <div class="modal-body">
                    You have successfully registered!
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-secondary" onclick = "$('#modalRegistered').hide()">Close</button>
                </div>
            </div>
        </div>
</c:if>

<c:if test="${success==0}">
        <div class=" modal-seen modal-dialog modal-dialog-centered" id="modalNotRegistered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitl">Something went wrong!</h5>
                </div>
                <div class="modal-body">
                 You didn't register :( Possibly email is already in use
                </div>
                <div class="modal-footer ">
                    <button type="button" class="btn btn-primary" onclick = "$('#modalNotRegistered').hide()">Try Again</button>
                    <a class="btn btn-primary center-block" href="home">Go to Home Page</a>
                </div>
            </div>
        </div>


</c:if>




</body>


<script src="./js/scripts.js"></script>
</html>
