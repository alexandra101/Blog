<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 05.07.2018
  Time: 14:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update article</title>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap-select.css">
    <script src="./js/bootstrap-select.bundle.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>

<c:import url="navbar.jsp"/>

<div class="container-fluid text-center mt-5">

    <h2>Update article</h2>

    <div class="container w-75 center-block text-justify">


        <form action="update-article?article=${article.articleId}" method="post"  enctype="multipart/form-data">
            <div class="form-group">
                <label for="articleTitle"> Title     </label>
                <input class="form-control w-100" name="title" type="text" id="articleTitle" placeholder="Enter article title" value="${article.title}" required pattern=".*{3,128}">
            </div>

            <div class="form-group">
                <label for="articleText">Text </label>
                <textarea class="form-control" name="text" id="articleText"> ${article.text}</textarea>
            </div>

            <div class="form-group">
                <label for="keywords">Select keywords</label>
                <select  required id="keywords" class="selectpicker" multiple name="keywords">
                    <c:forEach items="${keywords}" var="keyword">


                            <option  <c:forEach items="${article.keywords}" var="articleKeyword"> <c:if test="${keyword.keywordId==articleKeyword.keywordId}">  selected </c:if>  </c:forEach> style="padding: 0" value="${keyword.keywordId}">${keyword.keywordName}</option>

                        </c:forEach>
                </select>


                <c:out value="${keyword.keywordName}"/>
            </div>


            <img src="${article.imagePath}" class="img-thumbnail h-25 d-block">

            <div class="form-group border-primary bg-light mt-2">

                <label for="articleImage">Select another image</label>
                <input name="image" type="file" id="articleImage" accept="image/jpeg,image/png,image/gif">
                <label for="noImageCheckbox"> Or delete image</label>
                <input type="checkbox" id="noImageCheckbox" value="true" name="deleteImage">
            </div>
               <input name="submit" class="btn btn-success" type="submit" value="Edit" id="articleSubmit">

            <a href="profile" class="btn btn-secondary">Cancel</a>

        </form>
    </div>

</div>

<c:import url="footer.jsp"/>
</body>
</html>
