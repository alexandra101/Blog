function validateForm() {
    var password = $("#psw").val();
    var confirmPassword = $("#pswConf").val();

    return password === confirmPassword;
}


function checkPasswordMatch() {

    var password = $("#psw").val();
    var confirmPassword = $("#pswConf").val();
    if (password !== confirmPassword){
        $("#divCheckPasswordMatch").html("Passwords do not match!");
        $("#divCheckPasswordMatch").removeClass("alert-success");
        $("#divCheckPasswordMatch").addClass("alert-danger");

 }
    else{

        if(password===""){
            $("#divCheckPasswordMatch").html("Password cannot be empty.");
            $("#divCheckPasswordMatch").removeClass("alert-success");
            $("#divCheckPasswordMatch").addClass("alert-danger");

        } else{
        $("#divCheckPasswordMatch").html("Passwords match.");
        $("#divCheckPasswordMatch").removeClass("alert-danger");
        $("#divCheckPasswordMatch").addClass("alert-success");
        }}
}

function showPassword() {
    var passwordField = document.getElementById("psw");
    var  showPassword = document.getElementById("#showPasswordSpan");
    if (passwordField.type === "password") {
        passwordField.type = "text";
        showPassword.className="fa fa-eye-slash";


    } else {
        passwordField.type = "password";
        showPassword.className="fa fa-eye";
    }
}

$(document).ready(function () {
    $("#pswConf").keyup(checkPasswordMatch);
    $("#psw").keyup(checkPasswordMatch);


});


