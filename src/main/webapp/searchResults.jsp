<%--
  Created by IntelliJ IDEA.
  User: ADMIN
  Date: 11.07.2018
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Search results</title>

    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="./css/bootstrap/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css">
    <script src="./js/bootstrap.bundle.js"></script>
    <script src="./js/jquery-3.3.1.js"></script>
</head>
<body>


<c:import url="navbar.jsp"/>

<div class="background text-center mask rgba-black-light d-flex justify-content-center align-items-center">
    <h1 class="text-uppercase font-weight-bold ">Best blogging experience</h1>
</div>

<div class="w-75 container center-block text-center mt-4 pt-2">
    <h2 class="text-uppercase">Search results:</h2>


    <c:if test="${nothingIsFound!=null}">
        <h3>Sorry, no articles found</h3>
    </c:if>
    <c:forEach items="${articles}" var="article">
        <div class="card mt-5">

            <div class="card-header">
                <span class="text-uppercase font-weight-bold"><a class="card-link text-dark" href="article?article=${article.articleId}">${article.title}</a></span>
    <div>
            <c:forEach items="${article.keywords}" var="keyword">
                <span class="badge badge-dark">${keyword.keywordName}</span>
            </c:forEach>
    </div>
        <p class="text-secondary">by ${article.author.userLogin}</p>
        <p class="text-secondary">Published on: ${article.datePublished}</p>
        <p class="text-secondary">Updated on: ${article.dateUpdated}</p>

            </div>
            <div class="card-body">

                <div class="card-img mb-2">
                    <c:if test="${article.imagePath!=null}">
                        <img class="img-fluid h-50" src="${article.imagePath}">
                    </c:if>

                </div>

                <div class="card-text">
                    <p class="text-truncate text-justify">${article.text}</p>
                </div>

            </div>

        </div>
    </c:forEach>
</div>

<c:import url="footer.jsp"/>
</body>
</html>

