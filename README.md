# Another Blog

Multiuser web-application with ability to  add and edit articles.

# Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


## Prerequisites
1. Java v.1.8 or later
2. PostgreSQL v.10 or latter

## Installing

A step by step series of examples that tell you how to get a development env running

### Step 1: Setting up Database
1.Create new data base in pgAdmin
2.Open command line and navigate to the folder "bin" where it is installed PostgreSQL.

For Windows:
```
cd C:\Program Files\PostgreSQL\pg10\bin
```

3. Execute commands:

```
psql -U postgres DB_NAME < "PATH_TO_PROJECT\schema.sql"
psql -U postgres DB_NAME < "PATH_TO_PROJECT\data.sql"

```
Where: 

* DB_NAME - name of your created DB
* PATH_TO_PROJECT - path to the project

### Step 2: Setting DB properties
1. Open db-properties file
```
PATH_TO_PROJECT\src\main\resources\db.properties
```
2. Change properties to your values
```
user=USER_NAME
password=USER_PASSWORD
url=jdbc:postgresql://localhost:5432/DB_NAME
```
Where:

* DB_NAME - name of your created database
* USER_NAME - name of the user who has the right to edit your database
* USER_PASSWORD - user password

### Step 3: Build war file with Gradle
1. Open command line and go to the folder with project "PATH_TO_PROJECT"
2. Execute commands:
For Windows:
```
gradlew.bat war
```
### Step 4: Setting up Tomcat
1. Copy war-file from: ```PATH_TO_PROJECT\build\libs\``` to ```PATH_TO_TOMCAT\webapps```
2. Rename war-file to NEW_NAME.war, where NEW_NAME - prefix to project

**Remember to set up JAVA_HOME variable**



### Built With
1. Bootstrap 4
2. Gradle 

### Admin account:
```Email: user1@example.com```

```Password: Admin!1111```

### Author 

Alexandra Pervovanska